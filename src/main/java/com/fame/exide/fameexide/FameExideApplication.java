package com.fame.exide.fameexide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FameExideApplication {

	public static void main(String[] args) {
		SpringApplication.run(FameExideApplication.class, args);
	}

}
