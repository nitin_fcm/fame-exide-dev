package com.fame.exide.fameexide.config;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.fame.exide.fameexide.persistence.entity.User;
import com.fame.exide.fameexide.persistence.repository.UserRepository;

@Component
public class DataSetupLoader implements ApplicationListener<ContextRefreshedEvent>{

	private boolean alreadySetup = true;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if(alreadySetup)
			return;
		userRepository.save(User.builder()
				.email("nitin.k@in.fcm.travel")
				.firstName("Nitin")
				.lastName("Kumar")
				.password(passwordEncoder.encode("12345"))
				.username("nitin.k@in.fcm.travel")
				.isActive(true)
				.build());
	}
	

}
