package com.fame.exide.fameexide.config;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fame.exide.fameexide.persistence.entity.User;
import com.fame.exide.fameexide.service.UserService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JWTTokenProvider {
	
	@Autowired
	UserService userService;

	@Value("${app.jwtSecret}")
	private String jwtSecret;
	
	@Value("${app.jwtExpirationInMs}")
	private String jwtExpirationInMs;
	
	public String generateToken(User user) {
		Calendar date = Calendar.getInstance();
		Date now = date.getTime();
		date.add(Calendar.MINUTE, Integer.parseInt(jwtExpirationInMs));
		Date expiryDate = date.getTime();
		
		return Jwts.builder()
				.setSubject(Long.toString(user.getId()))
				.setIssuedAt(now)
				.setExpiration(expiryDate)
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}
	
	public Long getUserIdFromJWT(String token) {
		Claims claims = Jwts.parser()
				.setSigningKey(jwtSecret)
				.parseClaimsJws(token)
				.getBody();
		return Long.parseLong(claims.getSubject());
	}
	
	public boolean validateToken(String token) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
			return true;
		} catch (SignatureException e) {
			log.error("Invalid Signature : " + e.getMessage());
		} catch (MalformedJwtException e) {
			log.error("Invalid JWT token : " + e.getMessage());
		} catch (ExpiredJwtException e) {
			log.error("Expired JWT token : "+ e.getMessage());
		} catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
		return false;
	}
	
}
