package com.fame.exide.fameexide.config.security;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fame.exide.fameexide.persistence.entity.User;
import com.fame.exide.fameexide.persistence.repository.UserRepository;

@Service
@Transactional
public class FameUserDetailsService implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findAllByEmail(username);
		if(user == null)
			 new UsernameNotFoundException("User not found with email : " + username);
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				user.getIsActive(), true, true, true, new ArrayList<GrantedAuthority>());
	}
	
    public UserDetails loadUserById(Long id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with id : " + id));
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				user.getIsActive(), true, true, true, new ArrayList<GrantedAuthority>());
    }
	
	
	
}
