package com.fame.exide.fameexide.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Null;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="User")
public class User implements Serializable {
	
	private static final long serialVersionUID = 5733021562317544149L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@Column( name = "username", nullable = false)
	private String username;
	
	@Column( name = "password", nullable = false)
	private String password;
	
	@Column( name = "firstName", nullable = false)
	private String firstName;
	
	@Column( name = "lastName", nullable = false)
	private String lastName;
	
	@Column( name = "email", nullable = false)
	private String email;
	
	@Column( name = "isActive", nullable = false)
	private Boolean isActive;
	
	@Column( name = "createdOn", nullable = false)
	private Date createdOn;
	
	@Column( name = "modifiedOn", nullable = true)
	private Date modifiedOn;
	
	@PrePersist
	public void prePersist() {
		this.createdOn = new Date();
	}
	
	@PreUpdate
	public void preUpdate() {
		this.modifiedOn = new Date();
	}
	
}
