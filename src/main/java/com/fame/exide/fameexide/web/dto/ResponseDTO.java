package com.fame.exide.fameexide.web.dto;

import com.fame.exide.fameexide.persistence.entity.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ResponseDTO {
	Integer responseCode;
	String status;
	String token;
	User user;
}
