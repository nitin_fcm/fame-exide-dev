package com.fame.exide.fameexide.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fame.exide.fameexide.service.AuthenticationService;
import com.fame.exide.fameexide.web.dto.ResponseDTO;
import com.fame.exide.fameexide.web.dto.UserDTO;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/token")
public class AuthenticationController {
	
	@Autowired
	private AuthenticationService authenticationService;
	
	@RequestMapping(value = "/generate-token")
	public ResponseDTO register(@RequestBody UserDTO userDTO) {
		return authenticationService.getAuthToken(userDTO);
	}
	

}
