package com.fame.exide.fameexide.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fame.exide.fameexide.persistence.entity.User;
import com.fame.exide.fameexide.service.UserService;
import com.fame.exide.fameexide.service.common.StorageService;

@RestController
@RequestMapping(value="/api")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private StorageService storageService;
	
	@RequestMapping(value = "/users")
	public List<User> getUsers(){
		return userService.getAll();
	}
	
	@RequestMapping(value="/fileUpload")
	public void fileUpload(@ModelAttribute  MultipartFile[] files ) {
		for (MultipartFile file : files) {
			storageService.store(file, "F:\\angular\\fileUpload");
		}
	}
}
