package com.fame.exide.fameexide.service.common;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {
	String store(MultipartFile file, String uploadLocation);
	Resource loadAsResource(String filename);
}
