package com.fame.exide.fameexide.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import com.fame.exide.fameexide.config.JWTTokenProvider;
import com.fame.exide.fameexide.persistence.entity.User;
import com.fame.exide.fameexide.web.dto.ResponseDTO;
import com.fame.exide.fameexide.web.dto.UserDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AuthenticationService {
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired 
	private UserService userService;
	
	@Autowired
	private JWTTokenProvider jWTTokenProvider;
	
	public ResponseDTO getAuthToken(UserDTO userDTO) {
		String token = null;
		ResponseDTO responseDTO = null;
		User user  = null;
		try {
			System.out.println(">>> "+userDTO.toString());
			
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userDTO.getUsername(), userDTO.getPassword()));
			user = userService.getUserByEmail(userDTO.getUsername());
			System.out.println(user.toString());
			token = jWTTokenProvider.generateToken(user);
		} catch (Exception e) {
			log.error("Error while generating token : " + e.getMessage());
		}
		
		if(token != null) {
			responseDTO = ResponseDTO.builder()
					.responseCode(200)
					.status("success")
					.token(token)
					.user(user)
					.build();
		} else {
			responseDTO = ResponseDTO.builder()
					.responseCode(404)
					.status("failed")
					.build();
		}
		
		return responseDTO;
	}
}
