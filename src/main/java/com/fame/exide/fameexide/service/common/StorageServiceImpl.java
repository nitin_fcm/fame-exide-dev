package com.fame.exide.fameexide.service.common;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fame.exide.fameexide.service.error.StorageException;
import com.fame.exide.fameexide.service.error.StorageFileNotFoundException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StorageServiceImpl implements StorageService {


	@Override
	public String store(MultipartFile file, String uploadLocation) {
		final Path rootLocation = Paths.get(uploadLocation);
		String fileName = "";
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
			}
			fileName = file.getOriginalFilename();
			Files.copy(file.getInputStream(), rootLocation.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
			
		} catch (IOException e) {
			log.error("Failed to store file " + file.getOriginalFilename(), e);
			throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
		}
		return fileName;
	}

	@Override
	public Resource loadAsResource(String filename) {
		try {
			Resource resource = new UrlResource(Paths.get(filename).toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);
			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}
}
