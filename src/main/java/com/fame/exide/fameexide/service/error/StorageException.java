package com.fame.exide.fameexide.service.error;

public class StorageException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2519960184627823051L;

	public StorageException(String message) {
		super(message);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}

}
