package com.fame.exide.fameexide.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fame.exide.fameexide.persistence.entity.User;
import com.fame.exide.fameexide.persistence.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	public User getUserByEmail(String email) {
		User user = userRepository.findAllByEmail(email);
		if(user == null)
			throw new UsernameNotFoundException("User not found");
		return user;
	}
	 
	public User getUserById(Long id) {
		User user = userRepository.findById(id).get();
		if(user == null)
			throw new UsernameNotFoundException("User not found");
		return user;
	}
	
	public List<User> getAll(){
		return userRepository.findAll();
	}
}
